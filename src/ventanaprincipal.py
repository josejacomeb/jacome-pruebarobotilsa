#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Program by Jose Jacome josejacomeb@gmail.com
'''
Clase de ventana principal que inicializa y controla todos los elementos del QMainWindow.
'''
from datetime import datetime #Formatear feacha
from random import random #obtener personajes aleatoriamente

from PyQt5.QtWidgets import QMainWindow, QMessageBox, QMenu, QApplication
from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtGui import QCursor

from src.solicitarpersonajes import SolicitarPersonajes
from src.dialogo_secundaria import VentanaSecundaria

class VentanaPrincipal(QMainWindow):
    """
    Clase que hereda las funciones de QMainWindow e inicializa la interfaz
    ...

    Atributos
    ----------
    ui : object
        objeto generado por el programa pyuic5 y cargado en la interfaz principal
    url : str
        string que especifica el url deseado para hacer las peticiones
    personajes : int
        numero de personajes que se encuentran disponibles en la URL


    Métodos
    -------
    actualizar_hora(self)
        Actualiza la hora en el GUI
    solicitar_informacion_web(self)
        Genera 10 numeros aleatorios e inicia un nuevo Thread para solicitar la informacion
    actualizar_lista(self, diccionario_personajes)
        Recibe el diccionario de personajes recibido en el Thread y actualiza los
        keys del mismo para mostrar solo los nombres de los personajes
    def generar_error(self, mensaje_error)
        Si el Thread de Recibir informacion ha resultado en algún error, se genera
        un dialogo donde muestran los detalles del mismo
    actualizar_progreso(self, progreso)
        Actualiza la barra del progreso a traves desde una señal del Thread
    click_sobre_personaje(self, item)
        Slot que se activa cuando se presiona un elemento del QListWidget, verifica el click
        derecho y muestra una un popup para revisar informacion detallada del personaje
    abrir_dialogo_secundario(self, item)
        Una vez que se haya dado click en el Popup, abre un dialogo con la informacion de
        dicho personaje 

    """
    def __init__(self, ui, url="https://swapi.dev/api/people/", personajes=83):
        super(VentanaPrincipal, self).__init__() #Invocar el metodo padre
        self.ui = ui
        self.ui.setupUi(self) #Cargo el objeto dentro de la interfaz
        self.url = url #url para hacer el request por el Rest API
        self.numero_personajes = personajes #numero maximo de personajes
        self.diccionario_personajes = {} #Diccionario personajes

        #Establecer la fecha al inicio del programación
        self.fecha_hora_dt = datetime.now()
        fecha = self.fecha_hora_dt.strftime("%d/%m/%Y") #Formato dd/mm/yyyy"
        self.ui.fechaActual.setText(fecha)
        #Inicializar un QTimer para ajustar el tiempo cada segundo
        self.timerHora = QTimer()
        self.timerHora.timeout.connect(self.actualizar_hora) #Signal de tiempo acabado para actualizar
        self.timerHora.start(1000) #Inicializo con tiempo en ms
        #Conecto el Boton a un Slot cuando se da click
        self.ui.botonRequest.clicked.connect(self.solicitar_informacion_web)
        #Thread para solicitar los personajes en Web
        self.solicitarPersonajes = SolicitarPersonajes(self.url)
        self.solicitarPersonajes.emitirDiccionario.connect(self.actualizar_lista)
        self.solicitarPersonajes.senalError.connect(self.generar_error)
        self.solicitarPersonajes.progreso.connect(self.actualizar_progreso)
        #No mostrar progreso al inicio
        self.ui.barraProgreso.setHidden(True)
        self.ui.labelProgreso.setHidden(True)
        #Clickear item
        self.ui.listaPersonajes.itemPressed.connect(self.click_sobre_personaje)
        #Dialogo secundaria
        self.dialogo_secundaria = None
        self.actualizar_hora()

    def actualizar_hora(self):
        self.fecha_hora_dt = datetime.now()
        hora = self.fecha_hora_dt.strftime("%H:%M:%S") #Formato hh:mm:ss"
        self.ui.hora.setText(hora)

    def solicitar_informacion_web(self):
        personajes_indice_0 = self.numero_personajes - 1 #Contar desde 0 a 82
        #Generar 10 ids del rango 1 al 82
        ids_personajes = []
        for i in range(10):
            numero_aleatorio = int(random()*personajes_indice_0) + 1
            while numero_aleatorio in ids_personajes: #Checkear repetidos
                numero_aleatorio = int(random()*personajes_indice_0) + 1
            ids_personajes.append(numero_aleatorio)
        self.solicitarPersonajes.requerirWeb(ids_personajes)

    def actualizar_lista(self, diccionario_personajes):
        self.diccionario_personajes = diccionario_personajes
        #Agregar los keys de los personajes
        self.ui.listaPersonajes.clear() #Reinicializo el list view
        for personaje in self.diccionario_personajes.keys():
            self.ui.listaPersonajes.addItem(personaje)

    def generar_error(self, mensaje_error):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Hubo un fallo en la recepción de datos, "\
            "por favor verifique su conexión a internet e intente de nuevo")
        msg.setWindowTitle("Fallo en la solicitud de datos")
        msg.setDetailedText(mensaje_error)
        msg.exec()

    def actualizar_progreso(self, progreso):
        no_mostrar = None
        if progreso > 0 and progreso < 10:
            no_mostrar = False
        else:
            no_mostrar = True

        self.ui.barraProgreso.setHidden(no_mostrar)
        self.ui.labelProgreso.setHidden(no_mostrar)
        self.ui.barraProgreso.setValue(progreso) #Actualizar progreso

    def click_sobre_personaje(self, item):
        estadoRaton = QApplication.mouseButtons()
        if estadoRaton == Qt.RightButton:
            menuPersonaje = QMenu("Información Personaje", self)
            menuPersonaje.addAction("Información Personaje")
            menuPersonaje.triggered.connect(lambda: self.abrir_dialogo_secundario(item))
            menuPersonaje.exec(QCursor.pos())

    def abrir_dialogo_secundario(self, item):
        elementos = self.diccionario_personajes[item.text()]
        self.dialogo_secundaria = VentanaSecundaria()
        self.dialogo_secundaria.mostrar_elementos(elementos)
        self.dialogo_secundaria.exec()
