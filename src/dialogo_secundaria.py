#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Program by Jose Jacome josejacomeb@gmail.com
'''
Clase de ventana principal que inicializa y controla todos los elementos del Widget.
'''
from PyQt5.QtWidgets import QDialog

from src.ui_dialogo_secundaria import Ui_VentanaSecundaria


class VentanaSecundaria(QDialog):
    """
    Clase que hereda las funciones de QDialog e inicializa la Ventana Secundaria
    ...

    Atributos
    ----------

    Métodos
    -------
    mostrar_elementos(self, elementos) dict
        Muestra los elementso del diccionario individual que ha sido pasado como argumento
    """
    def __init__(self):
        super(VentanaSecundaria, self).__init__() #Invocar el metodo padre
        self.ui = Ui_VentanaSecundaria()
        self.ui.setupUi(self) #Cargo el objeto dentro de la interfaz
        self.elementos = {}

    def mostrar_elementos(self, elementos):
        self.elementos = elementos
        #cambio el nombre de la Ventana
        self.setWindowTitle("VS | {}".format(self.elementos["name"]))
        #Buscar si el diccionario este completo
        if "height" in self.elementos:
            self.ui.height.setText(self.elementos["height"])
        if "mass" in self.elementos:
            self.ui.mass.setText(self.elementos["mass"])
        if "hair_color" in self.elementos:
            self.ui.hair_color.setText(self.elementos["hair_color"])
        if "eye_color" in self.elementos:
            self.ui.eye_color.setText(self.elementos["eye_color"])
        if "birth_year" in self.elementos:
            self.ui.birth_year.setText(self.elementos["birth_year"])
        if "gender" in self.elementos:
            self.ui.gender.setText(self.elementos["gender"])
        if "skin_color" in self.elementos:
            self.ui.skin_color.setText(self.elementos["skin_color"])
