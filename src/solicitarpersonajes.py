#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Program by Jose Jacome josejacomeb@gmail.com
'''
TODO: Informacion
'''
from urllib.parse import urljoin #unir URLs

import requests #Libreria para manejar solicitudes de informacion en web
from PyQt5.QtCore import QThread, pyqtSignal

class SolicitarPersonajes(QThread):
    """
    Clase que hereda las funciones de QThread e inicializa solicitudes a la URL asignada
    ...

    Atributos
    ----------
    url : str
        string que especifica el url deseado para hacer las peticiones
    timeout : int
        tiempo de espera máximo para recibir la respuesta


    Métodos
    -------
    requerirWeb(self)
        Actualiza los ids a solicitar a la URL seleccionada
    run(self)
        Método sobrescrito, solicita la información en la web y maneja errores
    """
    senalError = pyqtSignal(str)
    emitirDiccionario = pyqtSignal(dict)
    progreso = pyqtSignal(int)

    def __init__(self, url, timeout = 10):
        QThread.__init__(self)
        self.url = url
        self.ids = []
        self.timeout = timeout

    def run(self):
        diccionario_personajes = {}
        error = ""
        str_error = ""
        self.progreso.emit(0) #Emite el inicio
        for contador, id in enumerate(self.ids):
            self.progreso.emit(contador + 1) #Emite la barra de progreso
            url_personaje = urljoin(self.url, str(id))
            try:
                #TODO Check despues de un error, recuperar la respuesta
                respuesta = requests.get(url_personaje, timeout=self.timeout) #Guardo en Formato diccionario_personajes
                respuesta = respuesta.json()
                if "name" in respuesta:
                    diccionario_personajes[respuesta["name"]] = respuesta
                else:
                    str_error = "No se pudo encontrar la key name en la respuesta \
                        del id: {} Respuesta servidor: {} URL: {}\n".format(id, respuesta, url_personaje)
                error += str_error
                str_error = ""
            except requests.exceptions.HTTPError as errh:
                str_error = "Error de HTTP: id: {} Detalles: {} \n".format(id, errh)
            except requests.exceptions.ConnectionError as errc:
                str_error = "Error de Conexión: id: {} Detalles: {} \n".format(id, errc)
            except requests.exceptions.Timeout as errt:
                str_error = "Error de Timeout: id: {} Detalles: {} \n".format(id, errt)
            except requests.exceptions.RequestException as err:
                str_error = "Error desconocido: id: {} Detalles: {} \n".format(id, err)
            error += str_error
        self.progreso.emit(10)
        if error:
            self.senalError.emit(error)
        else:
            self.emitirDiccionario.emit(diccionario_personajes)

    def requerirWeb(self, ids):
        self.ids = ids
        self.start()

    def __del__(self):
        self.wait() #Para el procesamiento antes de destruir el Thread
