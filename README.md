# Jácome-PruebaRobotilsa

Prueba para Robotilsa realizada por José Jácome - 2021

## Prueba Robotil S.A.
Interfaz gráfica de Usuario(GUI) creada por [José Jácome](https://www.linkedin.com/in/josejacomeb/) para la postulación al cargo de Ingeniero de Desarrollo de HW y SW.

## Descripción
Programa de una Interfaz gráfica en PyQT5 con conexión web para la descarga de nombres y características de los personajes de StarWars


## Imágenes
- Interfaz Principal

![Ventana Principal](media/ventana_principal.png)

- Ventana Secundaria

![Ventana Secundaria](media/ventana_secundaria.png)

## Instalación
El programa depende de dos librerías:

- `PyQt5==5.15.2` para la interfaz y
- `requests==2.26.0` para las solicitudes web.

Sino se encuentran las librerias presentes en el sistema(también la instalación por pip puede demorar algunos minutos), se puede instalar a través de PIP con el siguiente código:

```bash
pip install -r requirements.txt
```

## Uso
Para este programa en específico, solo basta con inicializarlo

```bash
python3 __main__.py
```

## Esqueleto del Código
La carpeta se encuentra distribuida de la siguiente forma:

- `jacome-pruebarobotilsa/`: Directorio principal del repositorio
  - `__main__.py`: Script principal del programa, el único que se debe ejecutar para este ejemplo
  - `src/`: Carpeta principal donde se encuentra el código auxiliar
    - `dialogo_secundaria.py`: QDialog donde se muestra la información de un personaje
    - `solicitarpersonajes.py`: Thread que solicita la información de los personajes en segundo plano y maneja errores
    - `ui_dialogo_secundaria.py`: UI generada por QT5 Designer y transformada por el script `pyuic5`
    - `ui_ventanaprincipal.py`: UI generada por QT5 Designer y transformada por el script `pyuic5`
    - `ventanaprincipal.py`: QMainWindow que carga la UI, gestiona los elementos y llama al Thread y al QDialog
  - `recurso_rc.py`: Archivo de recursos convertido a través de la conversión por el script `pyrcc5`
  - `recursos.qrc`: Archivo de recursos generado por Qt5 Designer
  - `ventana_secundaria.ui`: Archivo de la Ventana Secundaria generado en Qt5 Designer
  - `jacome-robotilsa.ui`: Archivo de la Ventana Principal generado en Qt5 Designer
  - `requirements.txt`: Archivo de las dependiencias del código
  - `README.md`: Breve documentación del proyecto.

## Contacto
José Jácome - [josejacomeb@gmail.com](mailto:josejacomeb@gmail.com)

## Opciones de Conversión

- Generar archivo recursos

    ```bash
    pyrcc5 recursos.qrc -o recursos_rc.py
    ```
- Generar archivo ui a py
  - UI Principal

      ```bash
      pyuic5 jacome-robotilsa.ui -o src/ui_ventanaprincipal.py
      ```
  - Dialogo Secundario

      ```bash
      pyuic5 ventana_secundaria.ui -o src/ui_dialogo_secundaria.py
      ```
