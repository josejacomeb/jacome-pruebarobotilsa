#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Program by Jose Jacome josejacomeb@gmail.com
'''
Programa principal para resolver el reto de programación de la Interfaz para
ROBOTILSA S.A.
'''

#Librerias a importar
import sys

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow

from src.ui_ventanaprincipal import Ui_VentanaPrincipal
from src.ventanaprincipal import VentanaPrincipal


def main():
    app = QApplication(sys.argv)
    ui = Ui_VentanaPrincipal()
    ventanaPrincipal = VentanaPrincipal(ui)
    ventanaPrincipal.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
